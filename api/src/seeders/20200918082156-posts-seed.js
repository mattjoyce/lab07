'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
     // Define dummy data for posts
    const posts = [
      {
        title: 'A silly post',
        content: 'Roses are red, violets are blue, I am a poet.',
        createdAt: new Date('2010/08/17 12:09'),
        updatedAt: new Date('2010/08/17 12:09')
      },
      {
        title: 'New technology',
        content: 'These things called "computers" are fancy.',
        createdAt: new Date('2011/03/06 15:32'),
        updatedAt: new Date('2011/03/06 15:47')
      },
      {
        title: 'Sample title',
        content: 'This is sample text.',
        createdAt: new Date('2012/01/10 17:22'),
        updatedAt: new Date('2012/01/10 17:22')
      },
      {
        title: 'Dummy post',
        content: 'Dummy text.',
        createdAt: new Date('2014/07/02 07:55'),
        updatedAt: new Date('2014/07/02 07:55')
      },
      {
        title: 'I am Matt',
        content: 'My name is Matt.',
        createdAt: new Date('2018/06/14 10:34'),
        updatedAt: new Date('2018/06/16 13:02')
      },
      {
        title: 'WDC',
        content: 'Web Development on the Cloud.',
        createdAt: new Date('2020/09/18 14:47'),
        updatedAt: new Date('2020/09/18 14:47')
      }
    ];
    // Insert posts into the database
    return queryInterface.bulkInsert('Posts', posts, {});
  },
  down: function (queryInterface, Sequelize) {
    // Delete all posts from the database
    return queryInterface.bulkDelete('Posts', null, {});
  }
};
